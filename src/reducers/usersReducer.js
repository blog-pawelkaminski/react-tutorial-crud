import { ADD_USER } from '../actions/usersActionsTypes';
import { REMOVE_USER } from '../actions/usersActionsTypes';

const initState = {
  
        "users" : [
            {"lastname" : "Kaminski", "email": "pawel.kaminski@email.com", "phone" : "12345679"},
            {"lastname" : "Radowski", "email": "radowski@email.com", "phone" : "12345679"},
            {"lastname" : "Wozniak", "email": "wozniak@email.com", "phone" : "12345679"},
            {"lastname" : "Kaminski2", "email": "pawel.kaminski@email.com", "phone" : "12345679"} ]

}
const usersReducer = (state = initState, action) => {
     if(action.type === ADD_USER){
        return {...state,
            users: [...state.users,action.user]
        }
     } else if(action.type === REMOVE_USER){
        let users = [...state.users];
        users.splice(action.id,1);
    
        return {...state,
            users: users
        }

     } 
    return state;
}
export default usersReducer;
    
   
