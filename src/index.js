import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import usersReducer from './reducers/usersReducer';
import registerServiceWorker from './registerServiceWorker';
import List from './List/List.js';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

const store = createStore(usersReducer);

ReactDOM.render(<Provider store={store}><List /></Provider>, document.getElementById('root'));
registerServiceWorker();
