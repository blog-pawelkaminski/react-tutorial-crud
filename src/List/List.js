import React, { Component } from 'react';
import Form from './Form/Form.js';
import { connect } from 'react-redux';
import { addUser } from '../actions/usersActions';
import { removeUser } from '../actions/usersActions';

class List extends Component {

 state = {
   "actualUser" : {"lastname" : "", "email": "", "phone" : ""}
  }

  	changeLastnameHandler = (event) => {
  		let obj = Object.assign({},this.state.actualUser);
  		obj.lastname = event.target.value;
	    this.setState({
	        "actualUser": obj,
	    });
    }

  	changeEmailHandler = (event) => {
  		let obj = Object.assign({},this.state.actualUser);
  		obj.email = event.target.value;
      	this.setState({
            "actualUser": obj,
      	});
    }

  	changePhoneHandler = (event) => {
  		let obj = Object.assign({},this.state.actualUser);
  		obj.phone = event.target.value;
	    this.setState({
	        "actualUser": obj,
	    });
    }

  	addHandler = (event) => { 
      this.props.add(this.state.actualUser);
    }

    deleteRecord = (event) => {
      let id = event.target.getAttribute("value"); 
      
      this.props.remove(id);
         
    }
 render() {

  
  	let c = 0;
  	let list = this.props.users.map(function(user,key){ 
  			if(key != 0) {
		  			return <tr key={key}>
		  				<td>{key}</td>
		  				<td >{user.lastname}</td>
		  				<td >{user.email}</td>
		  				<td >{user.phone}</td>
		  				<td value={key}  onClick={this.deleteRecord}>Usun</td>
	  				</tr>;
	  		}
  		},  this);



    return (
     <div className="container">
     	<h1>List of users:</h1>
     	<table className="table"><tbody>
     		{ list }
     		</tbody>
     	</table>
     	<div style={{'marginTop':'100px'}}></div>
     	<Form 	changeLastname={this.changeLastnameHandler}
     			changeEmail={this.changeEmailHandler}
     			changePhone={this.changePhoneHandler}
     			add={this.addHandler}/>
     	
     </div>
    );
  }


}

const mapStateToProps = (state)=>{
    return {
      users: state.users,
      actualUser: state.actualUser
    }
  }

const mapDispatchToProps = (dispatch) => {
  return {   
    add: (user) => {
      dispatch(addUser(user))
    },
    remove: (id) => {
      dispatch(removeUser(id))
    }
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(List)